#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define DATA_START 0x7A
#define HEADER_SIZE 54

class cTinyBMP{
    bool bInitialized;
    uint8_t *data;
    int Width,Height;
    uint8_t Header[HEADER_SIZE];
public:
    cTinyBMP(){
        bInitialized=false;
    }
    
    cTinyBMP(int w, int h){
        CreateHeader(w,h);
    }

    void CreateHeader(int w, int h){
        Width=w;
        Height=h;
        memset(&Header[0], 0x00, HEADER_SIZE);
        *(uint16_t*)&Header[0]=0x4D42;
        *(uint32_t*)&Header[10]=DATA_START;
        *(uint32_t*)&Header[14]=0x6C;
        *(uint32_t*)&Header[18]=w;
        *(uint32_t*)&Header[22]=h;
        *(uint16_t*)&Header[26]=1;
        *(uint16_t*)&Header[28]=24;
        *(uint32_t*)&Header[34]=w*h*3;
        data=(uint8_t*)malloc(*(uint32_t*)&Header[34]);
        bInitialized=true;
    }
    
    void Clear(bool b){
        if(!bInitialized)
            return;
        if(b)
            memset(data,0x00,Width*Height*3);
        else
            memset(data,0xFF,Width*Height*3);
    }
    void ClearColor(uint8_t r, uint8_t g, uint8_t b){
        for(int y=0;y<Height;y++){
            for(int x=0;x<Width;x++){
                *(uint8_t*)&data[(y*(Width*3)+x*3)]=b;
                *(uint8_t*)&data[(y*(Width*3)+x*3)+1]=g;
                *(uint8_t*)&data[(y*(Width*3)+x*3)+2]=r;
            }
        }
    }
    
    void SetPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b){
        y=(Height-1)-y;
        *(uint8_t*)&data[(y*(Width*3)+x*3)]=b;
        *(uint8_t*)&data[(y*(Width*3)+x*3)+1]=g;
        *(uint8_t*)&data[(y*(Width*3)+x*3)+2]=r;
    }
    
    void GetPixel(int x, int y, uint8_t *r, uint8_t *g, uint8_t *b){
        y=(Height-1)-y;
        *b=*(uint8_t*)&data[(y*(Width*3)+x*3)];
        *g=*(uint8_t*)&data[(y*(Width*3)+x*3)+1];
        *r=*(uint8_t*)&data[(y*(Width*3)+x*3)+2];
    }
    
    bool Save(const char* fname){
        if(!bInitialized)
            return false;
        FILE *f = fopen(fname,"wb");
        if(!f)
            return false;
        fwrite(&Header[0], 1, HEADER_SIZE, f);
        fseek(f, *(uint32_t*)&Header[10], SEEK_SET);
        fwrite(data, 1, *(uint32_t*)&Header[34], f);
        fclose(f);
        
        return true;
    }
    
    ~cTinyBMP(){
        if(bInitialized)
            free(data);
    }
};